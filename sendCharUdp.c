// Server side implementation of UDP client-server model 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

#define PORT	 35893 

// Driver code 
int main(int argc, char* argv[]) { 
  int sock;
  if(argc != 2) {
    printf("The program receives a single argument, a single character (ukBbTt), which it sends to  udp:127.0.0.1:%d\n", PORT);
  }
  char buf[1];
  buf[0] = argv[1][0]; 
  struct sockaddr_in srv;	
  if ( ( sock = socket(AF_INET, SOCK_DGRAM, 0) ) < 0 ) { 
    perror("Error creating the UDP(INET, DGRAM) socket."); 
    exit(EXIT_FAILURE); 
  }
  memset(&srv, 0, sizeof(srv)); 
  srv.sin_family = AF_INET; 
  srv.sin_addr.s_addr = inet_addr("127.0.0.1"); 
  srv.sin_port = htons(PORT); 
  sendto( sock, buf, 1, 0, (struct sockaddr*)&srv, sizeof(srv) );
  close(sock);
  return 0; 
} 
