#!/bin/bash
adjb=0.1
adjt=500

scriptName="pyRedlightBrightnessAdjuster.py"

fileName="/tmp/${scriptName}$(id -u).delta"

if [ -f "$fileName" ]; then
  l=$(cat "$fileName" | head -n 1)
  echo $l
  deltab=$(echo $l | cut -d' ' -f1)
  deltat=$(echo $l | cut -d' ' -f2)
else
  deltab=0
  deltat=0
fi

newb=0
newt=0

if [ $# -ne 1 ]; then
  echo "Please use a single argument, from B b T t 0b 0t u"
  exit 1
fi

case "$1" in
'b')
  newb=$(echo $deltab - $adjb | bc)
;;
'B')
  newb=$(echo $deltab + $adjb | bc)
;;
't')
  newt=$(echo $deltat - $adjt | bc)
;;
'T')
  newt=$(echo $deltat + $adjt | bc)
;;
'0b')
  newb=0
;;
'0t')
  newt=0
;;
esac

echo $newb $newt > "$fileName"
"$HOME"/.local/bin/"$scriptName" || DISPLAY=:0 xmessage "Could not run script $scriptName"
