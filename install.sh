#!/bin/bash
cd "$(dirname $0)"
gcc sctMod.c -Wall -std=c99 -lX11 -lXrandr -o sctMod || exit 1
gcc sendCharUdp.c -o sendCharUdp || exit 1


p3c=$(whereis -b pip3 | cut -d':' -f2 | wc -c)
pSc=$(whereis -b pip | cut -d':' -f2 | wc -c)
if [ $p3c -gt 2 ]; then
  echo "Found pip3."
  c="pip3"
else
  if [ $pSc -gt 2]; then
    echo "Haven't found pip3. Using pip."
    c="pip"
  else
    echo "Installing requires pip for python3, which was not found. Exiting."
    exit 1
  fi
fi
$c install --break-system-packages --user . && cp -fv pyRedlightBrightnessAdjuster.py sctMod sendCharUdp pyRBAChange.sh ~/.local/bin/ && mkdir -p ~/.config/pyRedlightBrightnessAdjuster && cp -v --update=none config.txt ~/.config/pyRedlightBrightnessAdjuster/
chmod +x ~/.local/bin/pyRedlightBrightnessAdjuster.py ~/.local/bin/pyRBAChange.sh
