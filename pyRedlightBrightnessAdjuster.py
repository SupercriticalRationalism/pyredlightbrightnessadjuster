#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable = bad-indentation, bad-whitespace, superfluous-parens, bad-continuation, invalid-name
'''This program adjust the screen white temperature and brightness in one go, using a customised version of sct. (.c file included)
 It runs both as a daemon and as a demo for the colour curve described in its configuration.
 It relies on files in its config directory.
 It also creates an udp server monitoring only the loopback interface (127.0.0.1), port 35893 . Writing a single character from B, b, T, t, u, k to udp:127.0.0.1:35893 increases/decreases the brightness, the temperature, forces an update or kills the daemon. (you can use the included .c utility).
No arguments: same as --noloop.
--daemon : run as a daemon.
--nonet  : run in background, without creating the network server, and without allowing interactive changes. The program simply follows the altitude / temperature curve.
--noloop : run only once. If the file /tmp/${programName}${uid}.delta exists, try applying the first number in the first line as the brightness delta, the second as the temp delta (on the same, single line). Noloop also outputs the current date to /tmp/${programName}${uid}.date , for verification purposes. 
--demo [brightness] : do a quick demo, going through the configured sun altitudes, at the set brightness (1 otherwise). Then exit immediately.
By popular demand, if the program doesn't detect a valid DISPLAY environment variables, it tries to connect to DISPLAY=:0. It needs to be run as the same user running the X11 server on that display.
Use the tool: ./genCronLine.sh to generate a line to add to /etc/crontab, for periodic running. Cron-looping should solve the issue the daemon and nonet modes have with suspending / resuming.
''' 

help = __doc__

import sys, os, os.path, subprocess
#for exit handlers (socket close)
import atexit
#network and single-thread concurrency
import socket, select

#for Path.home(), determining where ~ is.
from pathlib import Path

import math

#for config file parsing
import re

#for timezone and time-difference
import time, datetime

#for handily executing shell commands
import sh

#for determining the position of the sun relative to the horizon
import astropy.coordinates as coord
from astropy.time import Time, TimeDelta
import astropy.units as u

#~/.config/ is a standard path
configFileName = os.path.join(Path.home(), '.config/pyRedlightBrightnessAdjuster/config.txt')

#our custom port. should be out of the way. Currently, it's not IANA-allocated.
g_port = 35893
g_socket = None

g_sctBinary = 'sctMod'
#special meaning: '.' will be replaced with the path from where this script is run. Use './' for the actual current dir.
#~/.local/bin is also an expected path for custom user executables.
global g_sctAdditionalPaths
g_sctAdditionalPaths = [
  '.',
  os.path.join(Path.home(), '.local/bin')
]
global g_sctPath
g_sctPath = g_sctBinary

def updateSctPath():
  '''Looks for the g_sctBinary (the tool which updates the temperature and brightness) in $PATH, 
  and our custom-added paths'''
  global g_sctPath
  p = []
  #get $PATH from the environment, and split it according to its standard separator
  try:
    path = os.environ['PATH'].split(':')
  except:
    print('No PATH variable found in the environment! Trying local paths only.')
  #add our additional paths, if they're not duplicates
  for p in g_sctAdditionalPaths:
    if p not in path:
      path.append(p)
  #search if our binary exists in out paths,
  #  then exit and return it on the first find
  for p in path:
    pathToBinary = os.path.join(p, g_sctBinary)
    #isfile() returns true to links to files as well
    if os.path.isfile(pathToBinary):
      g_sctPath = pathToBinary
      print('Using', g_sctPath)
      return
    
#this decorator makes (or tries to make) the following function
#  execute when the script exits 
@atexit.register
def closeSocket():
  '''At exit cleanup for the socket (if used), so that the port can be reused.'''
  global g_socket
  if g_socket != None:
    g_socket.close()

#name convention: floatRe and not reFloat. since it's not a compiled re object
#nabbed off stackoverflow IIRC
floatRe     = r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'

reLongitude = re.compile(r'^\s*longitude\s*=\s*(' + floatRe + ')\s*', re.I)
reLatitude  = re.compile(r'^\s*latitude\s*=\s*(' + floatRe + ')\s*', re.I)
reLoopTime  = re.compile(r'^\s*looptime\s*=\s*(' + floatRe + ')\s*', re.I)
reDefaultBrightness = re.compile(r'^\s*defaultBrightness\s*=\s*(' + floatRe + ')\s*[#$]', re.I)
reBrightnessDelta = re.compile(r'^\s*brightnessDelta\s*=\s*(' + floatRe + ')\s*[#$]', re.I)
reAltTemp   = re.compile(r'^\s*(' + floatRe + ')\s*=\s*(' + floatRe + ')\s*$', re.I)

#the program does account for the timezone each time it loops (for long run times and DST), but does not account for changing positions (like travelling).
class RedlightBrightnessAdjuster:
  def __init__(self, configFileName):
    '''Initialises member variables and call the config file parser'''
    self.altTempMapping = []
    self.longitude = 0
    self.latitude = 0
    self.loopTime = 300
    self.defaultBrightness = 1
    self.brightnessDelta = 0.1
    self.tempDelta = 500
    self.adjBrightness = 0
    self.adjTemp = 0
    self.brightnessMin = 0.2
    self.brightnessMax = 2
    self.tempMin = 0
    self.tempMax = 6500
    self.brightness = 0
    self.brightnessAdjustmentDueToTemp = 0
    self.temp = 0
    self.loadConfig(configFileName)
    
  def loadConfig(self, configFileName):
    '''Parses the configuration file.'''
    with open(configFileName, 'r') as f:
      for line in f:
        #strip leading and following whitespaces
        line = line.strip()
        #if the line is empty or a comment, skip it
        if line.startswith('#') or len(line) == 0:
          continue
        #try to match each line to a format
        m = reLongitude.match(line)
        if m:
          #then extract the information
          self.longitude = float( m.group(1) )
          continue
        m = reLatitude.match(line)
        if m:
          self.latitude = float( m.group(1) )
          continue
        m = reLoopTime.match(line)
        if m:
          self.loopTime = float( m.group(1) )
          continue
        m = reDefaultBrightness.match(line)
        if m:
          self.defaultBrightness = float( m.group(1) )
          continue
        m = reBrightnessDelta.match(line)
        if m:
          self.brightnessDelta = float( m.group(1) )
          continue
        m = reAltTemp.match(line)
        if m:
          #this is more complex, since we assign a float to another
          #  the meaning is the left-hand side float is the Sun altitude
          #  and the RHS float is the white-temperature of the display
          print(m.group(0))
          alt  = float( m.group(1) )
          #because the float re messes up later groups
          temp =  m.group(0).split('=', 1)[-1].strip()
          self.altTempMapping.append([alt, float(temp.strip())])
          continue
    #we can't do anything if we don't have a mapping between Sun altitudes
    #  and temperatures
    if len(self.altTempMapping) == 0:
      print('Invalid configuration file. Exiting.')
      sys.exit(1)
    #sort the mapping according to Sun altitude
    self.altTempMapping = sorted(self.altTempMapping)
    self.print()

  def print(self):
    '''Prints the current settings (to stdout).'''
    print('long =', self.longitude,
          'lat =' , self.latitude, 
          'loopTime =', self.loopTime,
          'defaultBrightness =', self.defaultBrightness,
          'brightnessDelta =', self.brightnessDelta,
          'brightnessAdjustment =', self.adjBrightness,
          'currentBrightness = ', self.brightness,
          'tempDelta =', self.tempDelta,
          'tempAdjustment =', self.adjTemp,
          'currentTemp = ', self.temp,
          'brightnessAdjustmentDueToTemp = ', self.brightnessAdjustmentDueToTemp,
          'alt-temp mapping: ', self.altTempMapping
    )

  def shutdownOtherServer(self):
    '''Shutdown other instances of the daemon running on the local machine.'''
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
      #send the kill message
      sock.sendto( b'k', ('127.0.0.1', g_port) )
      #give the server time to shutdown and free the port
      time.sleep(3)
    except:
      pass
    #clean up
    sock.close()
    
  def run(self):
    '''Runs the UDP-server interactive loop.'''
    #g_socket is a global so we can close it properly with the
    #  atexit function
    global g_socket
    #make sure we are the only server running.
    #  shutdown other instances of the daemon
    self.shutdownOtherServer()
    #create an INET DGRAM socket (ipv4, UDP)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #bind to the loopback interface (address 127.0.0.1),
    #  so only local programs can contact the server.
    sock.bind( ( '127.0.0.1', g_port ) )
    #print the port we use, so people using custom UDP senders
    #  don't have to trudge through our global declarations.
    print('Listening at port', g_port)
    #assign the global variables, so the atexit function knows
    #  what to close
    g_socket = sock
    #Loop forever, exiting is an external event.
    while True:
      #Set the temp every time we loop.
      #  Computing the temp is relatively cheap, and we
      #  don't want to increase the time between updates.
      self.setTemp()
      #select blocks (pauses program / thread execution) until
      #  a descriptor is ready for non-blocking operations.
      #  The first parameter is the list of descriptors scanned 
      #  for reading (normal UDP read in our case),
      #  the second for writing (not used in our case),
      #  the third for exceptions (such as sudden socket shutdown).
      #  The fourth parameter is the maximum time select blocks for.
      #  In this case, select allows both reacting to network
      #  messages and a periodic timer, in a single thread.
      #  select also uses very little CPU.
      #  Select filters only the active (non-blocking if
      #  accessed now) descriptors from the first 3 parameters, 
      #  and returns that (in order).
      rl, _, xl = select.select([sock], [], [sock], self.loopTime)
      #if select returns only empty lists, then it timed-out,
      #  and we don't have anything to read from the socket.
      #  The self.setTemp() at the start of the while loop
      #  ensures the program updates.
      if len(rl) + len(xl) == 0:
        continue
      #Since we process the contents of the socket
      #  byte-by-byte, the loop ensures we can empty the
      #  socket each time it receives messages.
      while True:
        #Reat a single byte from the socket, and
        #  decode it as a single-char string.
        c = sock.recv(1).decode().strip()
        #If we received 0 bytes, then the socket is
        #  currently empty. We can break the read loop.
        if len(c) == 0:
          break
        elif c == 'k': #kill.
          g_socket.close()
          sys.exit(0)  #atexit closeSocket will clean up
        elif c == 'u': #update - unnecessary, but less surprising
          self.setTemp()
        elif c == 'b': #brightness down
          self.brightnessDecrease()
        elif c == 'B': #brightness up
          self.brightnessIncrease()
        elif c == 't': #temp down
          self.tempDecrease()
        elif c == 'T': #temp up
          self.tempIncrease()
       
  def runNoNet(self):
    '''Runs the non-interactive loop, only accounting for time.'''
    while True:
      self.setTemp()
      #sleep takes very little CPU
      time.sleep(self.loopTime)

  def oneshot(self, pathName):
    '''Runs the adjustment only once, optionally reading the delta and writing the run date/time from/to files'''
    fileDelta = pathName + '.delta'
    fileDate  = pathName + '.date'
    deltaB = 0
    deltaT = 0
    #first value in the file is the brightness delta, the second is the temperature delta
    if os.path.isfile(fileDelta):
      try:
        with open(fileDelta, 'r') as f:
          for l in f:
            l = l.strip()
            deltaB, deltaT = l.split()
            deltaB = float(deltaB)
            deltaT = float(deltaT)
            break
      except:
        print('could not read file')
        deltaB = 0
        deltaT = 0
    self.adjBrightness = deltaB
    self.adjTemp = deltaT
    self.setTemp()
    with open(fileDate, 'w') as f:
      f.write( 'deltaB: ' + str(deltaB) + ' deltaT: ' + str(deltaT) + ' time: ' + datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S') )
    
  def demo(self, brightness, delay = 0.2):
    '''Quickly go through the temperature curve, for testing / visualising.'''
    self.brightness = brightness
    #iterate from the lowest to the highest angle in our configuration 
    for alt in range(int(self.altTempMapping[0][0]), int(self.altTempMapping[-1][0]) + 1, 1):
      self.setTemp(alt)
      print('Demo: alt: ', alt, ' temp: ', self.temp, ' brightness: ', self.brightness, '\n\n')
      time.sleep(delay)
      
  def bariAvg(self, t, b1, b2):
    '''If b1 is one end of a line segment (position 0), b2 is the other (position 1),
    return the point at position t'''
    return b1 * (1 - t) + b2 * t
      
  def setTemp(self, alt = None):
    '''Computes both automatic and manual adjustments to gamma before calling the adjustment.'''
    if alt == None:
      alt = self.getSunAlt(self.longitude, self.latitude)
    temp = self.getTempFromSunAlt(alt)
    #add the computed and manually-adjusted temperatures
    self.temp = temp + self.adjTemp
    self.brightness = self.defaultBrightness + self.adjBrightness
    #clamp the values
    self.temp       = self.clamp(self.temp,       self.tempMin,       self.tempMax      )
    self.brightness = self.clamp(self.brightness, self.brightnessMin, self.brightnessMax)
    #change the display gamma
    print('-->t:', self.temp, '=', temp, '+', self.adjTemp) 
    print('-->b:', self.brightness, '=', self.defaultBrightness, '+', self.adjBrightness)
    self.setGamma(self.temp, self.brightness)
    self.print()
    
  def setGamma(self, temp, brightness):
    '''Calls the gamma-setting tool, with the temperature and brightness arguments.'''
    try:
      #os.system( g_sctPath + ' ' + str(temp) + ' ' + str(brightness) )
      subprocess.run([g_sctPath, str(temp), str(brightness)])
    except:
      #if we failed the previous attempt, then either we didn't find the binary tool
      #  or there are some errors with it running. We cannot do anything about the latter,
      #  so we try to fix the former, by re-scanning for the binary tool.
      print('Binary', g_sctPath, 'could not be found or executed. Looking again for it in $PATH and additional paths.')
      updateSctPath()
      #os.system( g_sctPath + ' ' + str(temp) + ' ' + str(brightness) )
      subprocess.run([g_sctPath, str(temp), str(brightness)])
                     
  def getSunAlt(self,
                longitude,
                latitude,
                time = None,
  ):
    '''Using Astropy, provides the position (from the config file) and time, 
    and returns the Altitude (as in, Altitude-Azimuth) of the Sun in that moment.
    Taken from the Astropy documentation.'''
    #position on the surface of the Earth
    loc = coord.EarthLocation(lon = longitude * u.deg,
                                lat = latitude * u.deg)
    #just legacy code from the Astropy example. We do want to find
    #  the Sun position now.
    if time is None:
      t = Time.now()
    else:
      t = Time(time)
    #an Altitude-Azimuth coordinate system, seeing the sky at the set
    #  (location, time) moment.
    altaz = coord.AltAz(location = loc, obstime = t)
    #the position of the Sun, now.
    sun = coord.get_sun(t)
    #we transfrorm the position of the Sun in our coordinate system,
    #  then we extract the Altitude.
    sunAlt = sun.transform_to(altaz).alt
    #we strip the degree unit, since our broader program
    #  doesn't properly use units, and assumes we use degrees.
    return(sunAlt / u.deg) 

  def getTempFromSunAlt(self, sunAlt):
    '''Using the configured colour curve, translates the Sun altitude into a white-temperature, 
    through clamped interpolation.'''
    print('Sun alt:', sunAlt)
    #fallthrough value
    temp = self.altTempMapping[0][1]
    #if the Sun dips below the lowest angle we've specified
    #  in the config, we clamp our temperature
    #  to that associated value
    if sunAlt < self.altTempMapping[0][0]:
      temp = self.altTempMapping[0][1]
    #same clamp as above if the Sun rises higher than the
    #  highest angle
    elif sunAlt > self.altTempMapping[-1][0]:
      temp = self.altTempMapping[-1][1]
    else:
      #if we're in-between, loop through the sorted angles
      for ii in range(len(self.altTempMapping) - 1):
        pAlt = self.altTempMapping[ii][0]
        nAlt = self.altTempMapping[ii + 1][0]
        #find between which values our current angle lies
        if pAlt <= sunAlt <= nAlt:
          #find at what distance out current angle lies
          prop = (sunAlt - pAlt) / (nAlt - pAlt)
          pTemp = self.altTempMapping[ii][1]
          nTemp = self.altTempMapping[ii + 1][1]
          #and interpolate the temperature using the same distance
          temp = prop * (nTemp - pTemp) + pTemp
          #we've found our temperature
          break
    #convert to int, to be sure.
    return( int(temp) )

  def brightnessIncrease(self, val = None):
    '''Increases the brightness in a clamped way.'''
    if val < 0:
      self.brightnessDecrease(val)
      return
    print('b+')
    if val == None:
      val = self.brightnessDelta
    #don't increase the brightness if it would push us over the max
    if (self.brightness + val) > self.brightnessMax:
      return
    #increase the brightness
    self.adjBrightness += val
    #the whiplash didn't look good, so it's disabled
    ##quickly increase back to the minimum value
    ##if self.brightness + self.adjBrightness < self.brightnessMin:
    ##  self.adjBrightness = -(self.brightness - self.brightnessMin)
    #clamp, to be sure
    self.adjBrightness = self.clamp(self.adjBrightness, -self.brightnessMax, self.brightnessMax)
    self.setTemp()

  def brightnessDecrease(self, val = None):
    '''Decreases the brightness in a clamped way.'''
    if val > 0:
      self.brightnessIncrease(val)
      return
    print('b-')    
    if val == None:
      val = self.brightnessDelta
    if (self.brightness - val) < self.brightnessMin:
      return
    self.adjBrightness -= val
    #quickly decrease back to the maximum value
    #if self.brightness + self.adjBrightness > self.brightnessMax:
    #  self.adjBrightness =  self.brightness - self.brightnessMax
    self.adjBrightness = self.clamp(self.adjBrightness, -self.brightnessMax, self.brightnessMax)
    self.setTemp()
    
  def tempIncrease(self, val = None):
    '''Increases the temperature in a clamped way.'''
    if val < 0:
      self.tempDecrease(val)
      return
    print('t+')
    if val == None:
      val = self.tempDelta
    if (self.temp + val) > self.tempMax:
      return
    self.adjTemp += val
    #quickly increase back to the minimum value
    #if self.temp + self.adjTemp < self.tempMin:
    #  self.adjTemp = self.tempMin - self.temp
    self.adjTemp = self.clamp(self.adjTemp, -self.tempMax, self.tempMax)
    self.setTemp()

  def tempDecrease(self, val = None):
    '''Decreases the temperature in a clamped way.'''
    if val > 0:
      self.tempIncrease(val)
      return
    print('t-')
    if val == None:
      val = self.tempDelta
    if (self.temp - val) < self.tempMin:
      return
    self.adjTemp -= val
    #quickly decrease back to the maximum value
    #if self.temp + self.adjTemp > self.tempMax:
    #  self.adjTemp = self.tempMax - self.temp
    self.adjTemp = self.clamp(self.adjTemp, -self.tempMax, self.tempMax)
    self.setTemp()

  def clamp(self, val, minimum, maximum):
    '''Clamps a value between 2 limits. Requirement: minimum <= maximum .'''
    if val < minimum:
      val = minimum
    if val > maximum:
      val = maximum
    return val
  
def main(args):
  '''Main function. Processes args, interfaces with the OS to get
  contextual information (like the UID), initialises global variables, 
  launches the temperature-updating loop.'''
  argc = len(args)
  #if we don't have a DISPLAY variable or if it's not set to an actual display 
  if 'DISPLAY' not in os.environ or os.environ['DISPLAY'] == '':
    print('Could not detect the DISPLAY variable in your environment. Defaulting to :0')
    #at least on Linux, python should update the environ if we update the os.environ dict.
    os.environ['DISPLAY'] = ':0'
    #for debugging feedback
    #os.system('xmessage "We set the display to :0"') 
  #the first parameter of args is always the pathname
  #  with which the program was launched.
  #  Python seems to magick it to work fine,
  #  even if launched like `python3 prog.py --arg`
  programName = os.path.basename(args[0])
  #search for the gamma-adjusting tool:
  #  first, replace '.' with the program-relative local path
  #  as we expect the tool to reside in the same directory
  global g_sctAdditionalPaths
  if '.' in g_sctAdditionalPaths:
    #remove '.'; use './' if you mean the actual current dir
    g_sctAdditionalPaths.remove('.')
    #get the dir the program was launched from
    newPath = os.path.dirname(args[0])
    #add that path if it's not a duplicate
    if newPath not in g_sctAdditionalPaths:
      g_sctAdditionalPaths.append( newPath )
  #rescan out updated paths for the tool
  updateSctPath()
  #get the current user's numeric ID (UID)
  uid = sh.id('-u').strip()
  #if we don't have any args (except the program launch path)
  # just run once.
  if argc == 1:
    tmpBasicPath = os.path.join( '/tmp', programName + str(uid) )
    loopingAdjuster = RedlightBrightnessAdjuster(configFileName)
    loopingAdjuster.oneshot(tmpBasicPath)
  elif argc > 1:
    #launches the quick demo of our config settings
    if args[1].lower() == '--demo':
      bri = 1
      #the second optional argument could be the custom brightness
      if argc > 2:
        #TODO: think. Should we allow for invalid second arguments, or just crash?
        try:
          bri = float(args[2])
        except:
          bri = 1
      loopingAdjuster = RedlightBrightnessAdjuster(configFileName)
      loopingAdjuster.demo(bri)
    #run the looping network-interactive daemon
    elif args[1].lower() == '--daemon':
      loopingAdjuster = RedlightBrightnessAdjuster(configFileName)
      loopingAdjuster.run()
    #run the simple loop, which only adjusts temperature according to time
    elif args[1].lower() == '--nonet':
      loopingAdjuster = RedlightBrightnessAdjuster(configFileName)
      loopingAdjuster.runNoNet()
    elif args[1].lower() == '--noloop':
      tmpBasicPath = os.path.join( '/tmp', programName + str(uid) )
      loopingAdjuster = RedlightBrightnessAdjuster(configFileName)
      loopingAdjuster.oneshot(tmpBasicPath)
    #if the first argument is something we don't handle (even -h or --help), print the help
    else:
      print(help)

#if the current script is launched, instead of being loaded as a module
#  then execute the main function, passing it the command-line arguments
if __name__ == '__main__':
  main(sys.argv)
