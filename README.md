# pyRedlightBrightnessAdjuster

Using an edited version of sct to edit both the white-temperature and brightness (on demand) on X11 displays, a python wrapper adjusts the white-balance towards lower temperatures (redder) as the altitude (AltAz) of the Sun drops.

# Capabilities
- Alter the white-temperature of monitor gamma on X11 servers (e.g. many Linux displays)
- Also alter the brightness at the same time. No flickering / overwrite.
- The colours are still vibrant, not muddled (due to sct).
- The temperature curve is defined in a config file.
- The temperature is defined as a function of Sun altitude (in an Alt-Az coordinate system).
- The adjustments are not sudden.
- The lowest gamma goes down to pure red, if needed (as a temperature of 0).
- The temperature and brightness can be interactively adjusted (the program daemonises and creates a small UDP server, bound to the loopback interface).
- The program can also run without the network server, looping without interactivity.
- It can also be run without looping at all; it reads adjustments from a file, `/tmp/${programName}${UID}.delta`. There is a tool included to adjust
- The program updates every 300 seconds, and does consumes extremely little CPU while waiting.
- Optional systemd user service installation (to deal with suspend / resume issues).

# Issues
- Not extensively tested.
- Fixed, user-only (but standard) install paths.
- The loop modes have issues with suspen/resume (it seems select.select gets stuck).
- I might not have included all the required python packages in the install script.

# Requirements
- GNU/Linux or a GNU-like operating system (tested only on Debian).
- X11
- Bash
- gcc
- python3
- pip
- Many python libraries, including Astropy.
- A loopback network interface enabled.
- Optional: a cron daemon or systemd.

# Install
- Run ./install.sh
It will compile the .c files, then it will copy the config to ~/.config/pyRedlightBrightnessAdjuster/config.txt, and the script and tools to ~/.local/bin
- Configure the program - your latitude and longitude, at the very least.
- You might need to manually add ~/.local/bin to PATH, if you want to more easily launch the programs herein.
- If you intend to run the --noloop version, you need to add it to, say, cron. The included genCronLine.sh script should output the required line. Add that either to /etc/crontab, to a file newly-created in /etc/cron.d, or (advanced users) in your user's crontab. This is a work-around for the suspend / resume bug. The specified cron user must be the same user owning the specified display (default :0).
- Optional: instead of cron, you can use systemd. Run ./installSystemd.sh to install and start the user-specific periodic jobs.

# Run
## Interactive
- pyRedlightBrightnessAdjuster --daemon
- Then, use `sendCharUdp B`, for instance, to increase the brightness. Bb increase/decrease brightness, Tt temperature, u triggers an update, k kills the daemon. This can be bount to hotkeys in you session manager.
- If you intend to use an appfinder or launch the script at session log-in, the DISPLAY variable needs to be set. The run.sh script contains a simple functional example of how to do it.
- The network server is specifically bound to the loopback interface (127.0.0.1), so it is not accessible from other machines.

## Non-interactive 
- pyRedlightBrightnessAdjuster --nonet
- The script simply follows the configured temperature curve throughout the day, without opening any network servers, nor allowing for interactive adjustments.

## noloop
- pyRedlightBrightnessAdjuster --noloop
- In addition to the computed temperature, brightness and temperature adjustments are red from `/tmp/${programName}${UID}.delta` (e.g. `/tmp/pyRedlightBrightnessAdjuster.py1000.delta`).
- The included ~/.local/bin/pyRBAChange.sh script adjusts these values. It receives a single argument (BbTtu) to increase/decrease brightness, temp, or just update.
- The file is not locked or multi-access protected (given this is a workaround for select misbehaviour). While unlikely to get bogus values, try to pause for a second between script invocations. The script should make corrections, but, if not, delete the .delta file
- In this mode, the program writes the parameters and time of its last execution to `/tmp/${programName}${UID}.delta` 

## Demo
- pyRedlightBrightnessAdjuster --demo
- This allows for the quick viewing of the colour curve programmed in the config file, for ease of configuration feedback

## Manually / in another tool
- `sctMod temperature [brightness]`

# Thanks
The sct tool is an amazing way of adjusting the gamma while preserving vibrant colours. Thanks go to the [author](https://www.tedunangst.com/flak/post/sct-set-color-temperature) 
 for making it and placing it in the Public Domain 