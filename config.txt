#this is the config file for the program Redlight Brightness Adjuster, a Python program which allows the redshifting of the colour temperature and the user-initiated brightness adjustment, in a single go (without flickering)
#lines starting with a # are ignored. leading and following whitespaces are ignored
longitude = 0
latitude = 51.8

#time delay, in seconds, to check the sun angle and adjust white temperature
looptime = 300

#screen default brightness. useful values are in [0, 2]
defaultBrightness = 1

#numbers mean: sunAltitude = whiteTemperature
#altitude is a concept from Alt-Az coordinates, meaning the angle between (the centre of) the object and the horizon
# you can use other values. The LHS number is an angle: degree, [0, 360].
# The RHS number is a temperature: Kelvin, [0, 1.416785(71)e32]. (you don't have a population-inversion monitor, I believe.)
# the list needn't be contiguous nor sorted, but it would improve the clarity of this file
-30 = 1000
-25 = 1500
-20 = 2000
-15 = 3000
 -5 = 5000
  0 = 6500

