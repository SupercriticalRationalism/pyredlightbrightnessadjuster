#!/bin/bash
systemctl --user disable pyRedlightBrightnessAdjuster.timer
systemctl --user disable pyRedlightBrightnessAdjuster.service
rm ~/.config/systemd/user/pyRedlightBrightnessAdjuster.timer
rm ~/.config/systemd/user/pyRedlightBrightnessAdjuster.service
systemctl --user daemon-reload
