#!/bin/bash
mkdir -p "${HOME}/.config/systemd/user"

echo "\
[Unit]
Description=python Redlight and Brightness Adjuster

[Service]
Environment=\"HOME=${HOME}\"
Environment=\"PATH=${PATH}:${HOME}/.local/bin\"
Environment=\"DISPLAY=${DISPLAY}\"
ExecStart=${HOME}/.local/bin/pyRedlightBrightnessAdjuster.py --noloop

[Install]
WantedBy=default.target
" > "${HOME}/.config/systemd/user/pyRedlightBrightnessAdjuster.service"

echo "\
[Unit]
Description=python Redlight and Brightness Adjuster timer

[Timer]
OnUnitActiveSec=60s


[Install]
WantedBy=timers.target
" > "${HOME}/.config/systemd/user/pyRedlightBrightnessAdjuster.timer"

systemctl --user daemon-reload
systemctl --user enable pyRedlightBrightnessAdjuster.service
systemctl --user enable pyRedlightBrightnessAdjuster.timer
systemctl --user start  pyRedlightBrightnessAdjuster.service
systemctl --user start  pyRedlightBrightnessAdjuster.timer
