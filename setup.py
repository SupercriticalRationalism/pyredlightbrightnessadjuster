#!/usr/bin/env python3
from pathlib import Path
import os.path
from distutils.core import setup

setup(name='RedlightBrightnessAdjuster',
      version='0.1',
      description='Redlight and Brigtness Monitor Adjustor for GNU',
      author='illPosedProblem',
      python_requires='>=3',
      install_requires=['pytz', 'tzlocal', 'astropy', 'sh'],
      requires=['pytz', 'tzlocal', 'astropy', 'sh'],
#      data_files=[
#        (os.path.join(Path.home(), '.local/bin'), ['pyRedlightBrightnessAdjuster.py']),
#        (os.path.join(Path.home(), '.config/pyRedlightBrightnessAdjuster'), ['config/config.txt'])
#        ]
      data_files=[],
     )
